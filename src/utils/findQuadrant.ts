export enum Quadrant {
  right = "RIGHT",
  left = "LEFT",
  top = "TOP",
  bottom = "BOTTOM",
}

const angleBetweenPoints = (
  x1: number,
  y1: number,
  x2: number,
  y2: number
): number => {
  const dx = x2 - x1;
  const dy = y2 - y1;
  const radians = Math.atan2(dy, dx);
  let degrees = (radians * 180) / Math.PI;
  if (degrees < 0) {
    degrees += 360;
  }
  return degrees;
};

export const findQuadrant = (
  x1: number,
  y1: number,
  x2: number,
  y2: number
): Quadrant => {
  const angle = angleBetweenPoints(x1, y1, x2, y2);
  if (angle >= 45 && angle < 135) {
    return Quadrant.bottom;
  }

  if (angle >= 135 && angle < 225) {
    return Quadrant.left;
  }

  if (angle >= 225 && angle < 315) {
    return Quadrant.top;
  }

  return Quadrant.right;
};
