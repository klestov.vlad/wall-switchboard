import { SVG } from "@svgdotjs/svg.js";
import { Svg } from "@svgdotjs/svg.js";
import { useEffect, useRef, useState } from "react";
import { useWallSwitchBoardDraw } from "../hooks/useWallSwitchBoardDraw";
import "@svgdotjs/svg.draggable.js";

export const DrawSvgCanvas = () => {
  const containerRef = useRef(null);
  const [drawContainer, setDrawContainer] = useState<Svg | null>(null);

  useWallSwitchBoardDraw({ drawContainer });

  useEffect(() => {
    if (!containerRef.current) return;
    const draw = SVG().addTo(containerRef.current).size(1000, 600);
    setDrawContainer(draw);

    return () => {
      draw.clear();
      draw.remove();

      drawContainer?.clear();
    };
  }, []);

  return <div ref={containerRef} />;
};
