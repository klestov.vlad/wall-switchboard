import { Rect, Line, Text, ForeignObject, Svg } from "@svgdotjs/svg.js";

import { useEffect, useRef, useState } from "react";
import { findQuadrant, Quadrant } from "../utils/findQuadrant";
import { useOnClickOutside } from "./useOnClickOutside";

const textRectParams = {
  width: 200,
  height: 30,
};

const wallSwitchParams = {
  width: 30,
  height: 30,
};

interface useWallSwitchBoardDrawProps {
  drawContainer: Svg | null;
}

export const useWallSwitchBoardDraw = ({
  drawContainer,
}: useWallSwitchBoardDrawProps) => {
  const [initialPoint, setInitialPoint] = useState<
    { x: number; y: number } | undefined
  >(undefined);

  const [wallSwitchText, setWallSwitchText] = useState("");
  const [isTextEditing, setIsTextEditing] = useState(true);
  const wallSwitchRef = useRef<Rect | null>(null);
  const textContainerRectRef = useRef<Rect | null>(null);
  const lineRef = useRef<Line | null>(null);
  const textRef = useRef<Text | null>(null);
  const foreignObjectRef = useRef<ForeignObject | null>(null);
  const wallSwitchPreviewRef = useRef<Rect | null>(null);

  const drawWallSwitchBoard = (x: number, y: number) => {
    setInitialPoint({ x, y });
  };

  const setCurrentText = () => {
    if (!wallSwitchText) return;
    setIsTextEditing(false);
  };

  useOnClickOutside("wall-switch-input", setCurrentText);

  useEffect(() => {
    if (!textRef.current) return;
    if (!textContainerRectRef.current) return;
    if (!drawContainer) return;
    if (!foreignObjectRef.current) return;

    if (!isTextEditing) {
      foreignObjectRef.current.remove();
      textRef.current?.tspan(wallSwitchText);

      textRef.current.move(
        textContainerRectRef.current.cx() - textRectParams.width / 2 + 4,
        textContainerRectRef.current.cy() - textRectParams.height / 2 + 6
      );
    }
  }, [foreignObjectRef.current, isTextEditing]);

  useEffect(() => {
    if (!drawContainer) return;
    if (initialPoint) {
      wallSwitchPreviewRef.current?.remove();
      return;
    }

    const wallSwitchPreview = drawContainer
      .rect(wallSwitchParams.width, wallSwitchParams.height)
      .fill("grey")
      .opacity(0.5)
      .move(0, 0);

    wallSwitchPreviewRef.current = wallSwitchPreview;

    drawContainer.on("mousemove", (event) => {
      //@ts-ignore
      wallSwitchPreview.move(event.x, event.y);
    });

    return () => {
      drawContainer.off("mousemove");
    };
  }, [drawContainer, initialPoint]);

  useEffect(() => {
    if (!drawContainer) return;

    drawContainer.on("click", (event) => {
      if (initialPoint) return;
      //@ts-ignore
      drawWallSwitchBoard(event.x, event.y);
    });

    if (!initialPoint) return;

    drawContainer.off("click");

    const wallSwitch = drawContainer
      .rect(wallSwitchParams.width, wallSwitchParams.height)
      .fill("grey")
      .move(initialPoint.x, initialPoint.y);

    const textRectContainer = drawContainer
      .rect(textRectParams.width, textRectParams.height)
      .addClass("wall-switch-text-container")
      .move(initialPoint.x + 100, initialPoint.y - 100);

    const line = drawContainer
      .line(
        wallSwitch.cx() + wallSwitchParams.width / 2,
        wallSwitch.cy(),
        textRectContainer.cx() - textRectParams.width / 2,
        textRectContainer.cy()
      )
      .stroke({ color: "grey", width: 1, linecap: "round" });

    wallSwitchRef.current = wallSwitch;
    textContainerRectRef.current = textRectContainer;
    lineRef.current = line;

    function handleMove() {
      const quadrant = findQuadrant(
        wallSwitch.cx(),
        wallSwitch.cy(),
        textRectContainer.cx(),
        textRectContainer.cy()
      );

      const xShiftCoefficient =
        quadrant === Quadrant.right ? -1 : quadrant === Quadrant.left ? 1 : 0;

      const yShiftCoefficient =
        quadrant === Quadrant.bottom ? -1 : quadrant === Quadrant.top ? 1 : 0;

      line.plot(
        wallSwitch.cx() - (xShiftCoefficient * wallSwitchParams.width) / 2,
        wallSwitch.cy() - (yShiftCoefficient * wallSwitchParams.height) / 2,
        textRectContainer.cx() + (xShiftCoefficient * textRectParams.width) / 2,
        textRectContainer.cy() + (yShiftCoefficient * textRectParams.height) / 2
      );

      textRef.current?.move(
        textRectContainer.cx() - textRectParams.width / 2 + 5,
        textRectContainer.cy() - textRectParams.height / 2 + 5
      );
    }

    wallSwitch.draggable().on("dragmove", handleMove);
    textRectContainer.draggable().on("dragmove", handleMove);

    return () => {
      drawContainer.clear();
      drawContainer.remove();
      drawContainer.off("click");
    };
  }, [drawContainer, initialPoint]);

  useEffect(() => {
    if (!drawContainer) return;
    if (!initialPoint) return;
    if (!isTextEditing) return;

    const foreignObjectCoordinate = {
      x: textContainerRectRef.current?.cx()
        ? textContainerRectRef.current?.cx() - textRectParams.width / 2
        : initialPoint.x + 100,
      y: textContainerRectRef.current?.cy()
        ? textContainerRectRef.current?.cy() - textRectParams.height / 2
        : initialPoint.y - 100,
    };

    const foreignObject = drawContainer
      .foreignObject(textRectParams.width, textRectParams.height)
      .move(foreignObjectCoordinate.x, foreignObjectCoordinate.y);

    const input = document.createElement("input");
    input.setAttribute("type", "text");
    input.setAttribute("placeholder", "input text and press enter");
    input.setAttribute("style", "width: 100%; height: 100%;");
    input.setAttribute("id", "wall-switch-input");
    input.setAttribute("value", wallSwitchText);

    //@ts-ignore
    foreignObject.add(input);

    if (!textRef.current) {
      const text = drawContainer
        .text(wallSwitchText)
        .font({
          size: 14,
          lineHeight: 14,
          textAlign: "center",
          with: 160,
          textOverflow: "ellipsis",
        })
        .addClass("wall-switch-text");

      text.on("click", () => {
        setIsTextEditing(true);
        input.focus();
      });

      textRef.current = text;
    }

    input.addEventListener("keyup", (event) => {
      setWallSwitchText(input.value);
      if (event.key === "Enter" && input.value) {
        setIsTextEditing(false);
      }
    });

    const end = input.value.length;
    input.setSelectionRange(end, end);
    input.focus();

    foreignObjectRef.current = foreignObject;

    return () => {
      foreignObject.remove();
      input.removeEventListener("keyup", () => {});
      input.remove();
    };
  }, [drawContainer, initialPoint, isTextEditing]);
};
