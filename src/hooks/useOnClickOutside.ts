import { useEffect } from "react";

type Handler = (event: MouseEvent) => void;

export function useOnClickOutside<T extends HTMLElement = HTMLElement>(
  id: string,
  handler: Handler,
  mouseEvent: "mousedown" | "mouseup" = "mousedown"
): void {
  useEffect(() => {
    document.addEventListener(mouseEvent, (event) => {
      const el = document.getElementById(id);

      if (!el || el.contains(event.target as Node)) {
        return;
      }

      handler(event);
    });

    return () => {
      document.removeEventListener(mouseEvent, handler);
    };
  }, [id, handler]);
}
