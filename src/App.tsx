import React from "react";
import { DrawSvgCanvas } from "./components/drawSvgCanvas";
import roomImg from "./images/room.png";
import "./App.css";

function App() {
  return (
    <div className="App">
      <img
        className="App-image"
        src={roomImg}
        width="100%"
        height="100%"
        alt="room"
      />
      <DrawSvgCanvas />
    </div>
  );
}

export default App;
